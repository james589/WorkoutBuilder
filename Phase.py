from misc_funcs import remove_empty_space, delim_listofstr, read_workout_line
from Pulse import Pulse
from Timeslot import TimeSlot

from collections import OrderedDict


class Phase:
    """
    Phase
        Duration (s), (fp)
        Start delay (s), (fp)
        Ramp-up threshold (fp)
        Ramp up (s), (fp)
        Contraction (s), (fp)
        Ramp-down threshold (fp)
        Ramp down (s), (fp)
        Relaxation (s), (fp)
        End delay (s), (fp)
        Frequency (Hz), (fp)  [≤ 120Hz of the predicate.  In practice this will be much lower]
        Pulses (integer)

    PH1,300,0,90,90,0,90,90,0,0,4.0,3
    PL1,B,100,1,2000
    TS1,C,300,5,30,4,88,22
    PL2,B,100,1,500
    TS1,C,400,5,35,2,80,08
    TS2,C,180,5,50,2,08,04
    TS3,C,180,5,50,2,08,01
    """

    identifier = "PH"
    terms = [
        "Duration (s)",
        "StartDelay (s)",
        "RampUpThreshold (%)",
        "RampUpDuration (s)",
        "Contraction (s)",
        "RampDownThreshold (%)",
        "RampDownDuration (s)",
        "Relaxation (s)",
        "EndDelay (s)",
        "Frequency (Hz)",
        "Pulses",
    ]

    term_types = {
        "Duration (s)": float,
        "StartDelay (s)": float,
        "RampUpThreshold (%)": float,
        "RampUpDuration (s)": float,
        "Contraction (s)": float,
        "RampDownThreshold (%)": float,
        "RampDownDuration (s)": float,
        "Relaxation (s)": float,
        "EndDelay (s)": float,
        "Frequency (Hz)": float,
        "Pulses": int,
    }

    def __init__(self, s: str, line_sep: str = "\n", sep: str = ",") -> None:

        if type(s) == str:
            s = remove_empty_space(s)
            s_lines = s.split(line_sep)
        elif type(s) == list and type(s[0] == str):
            s_lines = s
        else:
            raise TypeError("s can only be a list of strings or a string")

        try:
            self.id, self.attribs = read_workout_line(
                s_lines[0], self.identifier, self.terms, sep
            )

            self.pulses = [
                Pulse(pulse_str)
                for pulse_str in delim_listofstr(s_lines[1:], Pulse.identifier)
            ]

        except ValueError as err:
            print(f"Error: {err}")

        # Cast the attribute strings read in, into the correct type
        temp = OrderedDict()
        for attrib_name, attrib in self.attribs.items():
            temp[attrib_name] = self.term_types[attrib_name](attrib)
        self.attribs = temp

        try:
            self.valid()
        except AssertionError as err:
            print(f"Phase {self.id} is Invalid - Errors: {err}")

    def __len__(self):
        return len(self.pulses)

    def __repr__(self):
        # return f"Phase_{self.id}: \nAttributes: {self.attribs} \nContains {len(self.pulses)} pulses" OLD
        # f"{self.attribs['Duration (s)']}s duration ({int(self.attribs['Duration (s)'])/60:.0f}mins)", OLD
        # return f"Phase {self.id}: " + ", ".join(
        #     [f"{self.attribs[term]} {term}" for term in self.terms]
        # )

        return "\n".join(
            [
                f"Phase {self.id}: ",
                ", ".join([f"{self.attribs[term]} {term}" for term in self.terms][:4]),
                ", ".join([f"{self.attribs[term]} {term}" for term in self.terms][4:8]),
                ", ".join([f"{self.attribs[term]} {term}" for term in self.terms][8:]),
                *[str(pulse) for pulse in self.pulses],
            ]
        )

    @property
    def pulses_dict(self) -> dict:
        pulses_dict = OrderedDict()
        for pulse in self.pulses:
            pulses_dict.update(pulse.to_dict())
        return pulses_dict

    def to_dict(self):
        output = OrderedDict(**self.attribs).copy()
        output["Pulses"] = self.pulses_dict
        return OrderedDict([[f"Phase_{self.id}", output]])

    def valid(self) -> bool:
        valid = True
        errors = list()

        # Consecutive Pulse numbers
        pulse_nums = [pulse.id for pulse in self.pulses]
        if pulse_nums != [i + 1 for i in range(len(self.pulses))]:
            valid = False
            errors.append("Pulse ids in phase are not consecutive")

        # Num Phases match Workout desc
        if len(self.pulses) > self.attribs["Pulses"]:
            valid = False
            errors.append(
                f"There are more Pulses present in Phase {self.id} than is declared on the Phase desc line"
            )

        if len(self.pulses) < self.attribs["Pulses"]:
            valid = False
            errors.append(
                f"There are less Pulses present in Phase {self.id} than is declared on the Phase desc line"
            )

        if not valid:
            raise AssertionError(errors)


def self_test(VERBOSE=False):
    from Workout_sample import bionic_quads
    from pprint import pprint

    PH1 = Phase(bionic_quads.split("\n")[1:27])  # First Phase from BionicQuads
    output_data = PH1.to_dict()

    if VERBOSE:
        print(PH1)
        # pprint(output_data) # this is broken


if __name__ == "__main__":
    try:
        self_test(True)
    except AssertionError:
        print(AssertionError)
