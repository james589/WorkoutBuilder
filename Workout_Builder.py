from misc_funcs import print_header
from Workout import Workout
import os


def convert_oldworkout_to_newformat(fname: str, fname_out: str = None) -> None:
    """Take an old format workout file and convert it to yaml"""

    if fname_out is None:
        fname_out = f"{fname.split('.')[:-1]}_NEW.yaml"

    with open(fname) as f:
        old_fstr = f.read()

    workout = Workout(old_fstr)
    new_fstr = workout.to_yaml()

    with open(fname_out) as f_out:
        f.write(new_fstr)


def test_timeslotduration(VERBOSE=False):
    from Workout_sample import bionic_quads

    WO1 = Workout(bionic_quads)
    if VERBOSE:
        print("loaded bionic quads")

    bq_ph1_pl1_ts1 = WO1.phases[0].pulses[0].timeslots[0]
    if VERBOSE:
        print(f"{bq_ph1_pl1_ts1.duration=}")
    assert bq_ph1_pl1_ts1.duration == 305


def selftest(VERBOSE=False) -> None:
    if VERBOSE:
        print_header("Beginning Self test")

    try:
        import sys

        test_timeslotduration()

    except AssertionError:
        print_header("Failed tests")
        sys.exit()


def main() -> None:
    selftest()

    from misc_funcs import read_in_workoutfile, output_workout_obj_repr_to_file

    s, fpath = read_in_workoutfile(
        "Snippets\\Measurement\\LHamstring_800us_FLIPPING_V0.02.csv"
    )
    WO1 = Workout(s)

    output_workout_obj_repr_to_file(
        WO1,
        fpath,
        output_subfolder="HumanReadable",
        extension="_HR.txt",
    )

    print(WO1)

    # WO1.to_yaml()

    print_header("SCRIPT FINISHED SUCCESSFULLY")


if __name__ == "__main__":
    main()
