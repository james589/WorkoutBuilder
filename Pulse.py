from ast import Assert, expr_context
from misc_funcs import remove_empty_space, read_workout_line, indent_paragraph
from collections import OrderedDict
from Timeslot import TimeSlot


class Pulse:
    """
    PL5,B,100,6,500
    TS1,C,180,5,50,4,0A,05
    TS2,C,180,5,50,2,08,04
    TS3,C,180,5,50,2,08,01
    TS4,D,30,5,50,2,01,08
    TS5,C,180,5,50,2,04,02
    TS6,C,180,5,50,2,04,08
    """

    identifier = "PL"
    terms = [
        "Type",
        "Biphasic gap (us)",  # µs
        "NumTimeSlots",
        "Pre-pulse delay (us)",
    ]  # µs

    term_types = {
        "Type": str,
        "Biphasic gap (us)": float,  # µs
        "NumTimeSlots": int,
        "Pre-pulse delay (us)": float,
    }

    def __init__(self, s: str, line_sep: str = "\n", sep: str = ",") -> None:

        if type(s) == str:
            s = remove_empty_space(s)
            s_lines = s.split(line_sep)
        elif type(s) == list and type(s[0] == str):
            s_lines = s
        else:
            raise TypeError("s can only be a list of strings or a string")

        try:
            self.id, self.attribs = read_workout_line(
                s_lines[0], self.identifier, self.terms, sep
            )
            self.timeslots = [TimeSlot(line) for line in s_lines[1:]]
        except ValueError as err:
            print(f"Error: {err}")

        # Cast the attribute strings read in, into the correct type
        temp = OrderedDict()
        for attrib_name, attrib in self.attribs.items():
            temp[attrib_name] = self.term_types[attrib_name](attrib)
        self.attribs = temp

        try:
            self.valid()
        except AssertionError as err:
            print(f"Invalid Pulse: {self.id} - Errors: {err}")

    def num_timeslots(self) -> int:
        return self.__len__()

    def __len__(self) -> int:
        return len(self.timeslots)

    @property
    def pulse_type(self):
        pulse_types = {"B": "BiPhasic", "M": "MonoPhasic"}

        return pulse_types[self.attribs["Type"]]

    def __repr__(self):
        # return f"Pulse_{self.id}: \nAttributes: {self.attribs} \nContains {len(self)} timeslots" OLD
        # return f"Pulse {self.id}: " + ", ".join(
        #     [f"{self.attribs[term]} {term}" for term in self.terms]
        # )

        repr_str = "\n".join(
            [
                f"Pulse {self.id}: {self.pulse_type}",
                ", ".join([f"{self.attribs[term]} {term}" for term in self.terms[1:]]),
                *[str(timeslot) for timeslot in self.timeslots],
            ]
        )
        repr_str = indent_paragraph(repr_str, indents=1)
        return repr_str

    @property
    def timeslots_dict(self) -> dict:
        timeslots_dict = OrderedDict()
        for timeslot in self.timeslots:
            timeslots_dict.update(timeslot.to_dict())
        return timeslots_dict

    @property
    def duration(self) -> int:
        timeslots_dict = OrderedDict()
        for timeslot in self.timeslots:
            timeslots_dict.update(timeslot.to_dict())
        return timeslots_dict

    def to_dict(self) -> dict:
        output = OrderedDict(**self.attribs).copy()
        output["TimeSlots"] = self.timeslots_dict
        return OrderedDict([[f"Pulse_{self.id}", output]])

    def valid(self) -> bool:
        valid = True
        errors = list()

        # Consecutive Timeslot numbers
        timeslots_nums = [timeslot.id for timeslot in self.timeslots]
        if timeslots_nums != [i + 1 for i in range(len(self.timeslots))]:
            valid = False
            errors.append("Timeslots ids in pulse are not consecutive")

        # Num timeslots match Pulse desc
        if self.attribs["NumTimeSlots"] > len(self.timeslots):
            valid = False
            errors.append(
                f"There are more timeslots present in Pulse {self.id} than is declared on the Pulse desc line"
            )

        if self.attribs["NumTimeSlots"] < len(self.timeslots):
            valid = False
            errors.append(
                f"There are less timeslots present in Pulse {self.id} than is declared on the Pulse desc line"
            )

        if not valid:
            raise AssertionError(errors)


def self_test(VERBOSE=False):
    print("PULSE self_test()")

    from pprint import pprint

    from Workout_sample import bionic_quads

    PL1 = Pulse(bionic_quads.split("\n")[2:5])  # First Pulse from BionicQuads

    output_data = PL1.to_dict()

    if VERBOSE:
        print(PL1)
        pprint(output_data)

    from Workout_sample import pulse_teststring_err

    # check errors are caught correctly
    try:
        PL1 = Pulse(pulse_teststring_err)
    except AssertionError as err:
        if (
            repr(err)
            == """AssertionError("Invalid Pulse: 1 - Errors: ['Timeslots ids in pulse are not consecutive']")"""
        ):
            print("Timeslot non consecutive Error caught successfully")
        else:
            print("TEST FAILED: ERROR NOT CAUGHT")


if __name__ == "__main__":
    try:
        self_test()
    except AssertionError:
        print(AssertionError)
