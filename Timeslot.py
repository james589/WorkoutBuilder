from collections import OrderedDict
from misc_funcs import read_workout_line, indent_paragraph


class TimeSlot:
    """
    "TS5,C,180,5,50,2,04,02"
    "TimeslotName, Type, Duration(us), Deadtime(us), Amplitude (%), Num_Electrodes, Positive Electrodes, Negative Electrodes"
    """

    electrode_lut = {
        "00": "Off",
        "01": "Left_LowerHamstring",
        "02": "Left_LowerQuadricep",
        "04": "Left_UpperHamstring",
        "08": "Left_UpperQuadricep",
        "10": "Right_LowerHamstring",
        "20": "Right_LowerQuadricep",
        "40": "Right_UpperHamstring",
        "80": "Right_UpperQuadricep",
    }

    identifier = "TS"
    terms = [
        "Type",
        "Duration (us)",  # µs
        "DeadTime (us)",  # µs
        "Amplitude (%)",
        "NumElectrodes",
        "Pos_Electrodes",
        "Neg_Electrodes",
    ]

    term_types = {
        "Type": str,
        "Duration (us)": float,  # µs
        "DeadTime (us)": float,  # µs
        "Amplitude (%)": float,
        "NumElectrodes": int,
        "Pos_Electrodes": str,
        "Neg_Electrodes": str,
    }

    @property
    def pulse_type(self):
        pulse_types = {"C": "Contraction", "D": "Discharge"}

        return pulse_types[self.attribs["Type"]]

    def __init__(self, s: str, sep: str = ",") -> None:
        self.id, self.attribs = read_workout_line(s, self.identifier, self.terms, sep)
        temp = OrderedDict()

        for attrib_name, attrib in self.attribs.items():
            temp[attrib_name] = self.term_types[attrib_name](attrib)
            # temp[attrib] = list(map(self.term_types[attrib_name], [attrib]))[0]
        self.attribs = temp

    def __repr__(self) -> str:
        electrodes = f"{self.electrode_lut[self.attribs['Pos_Electrodes']]} to {self.electrode_lut[self.attribs['Neg_Electrodes']]}"

        repr_str = (
            ", ".join([f"{self.attribs[term]} {term}" for term in self.terms[1:4]])
            + ", \n"
            + ", ".join([f"{self.attribs[term]} {term}" for term in self.terms[4:]])
            + f", \n{electrodes}"
        )
        repr_str = indent_paragraph(repr_str, indents=1)

        repr_str = f"Timeslot {self.id}:  {self.pulse_type}\n" + repr_str
        repr_str = indent_paragraph(repr_str, indents=1)

        return repr_str

    def to_dict(self) -> dict:
        return {f"Timeslot_{self.id}": self.attribs}

    def valid(self) -> bool:
        """Checks if the object is a valid instance"""
        valid_types = ["C", "D"]
        two_electrode_vals = [
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "00",
            "10",
            "20",
            "30",
            "40",
            "50",
            "60",
            "70",
            "80",
        ]

        three_electrode_vals = []
        errors = list()
        attrs = self.attribs

        err_msgs = {
            "type": f"Type passed is not valid, \n\t\
            Passed: {attrs['Type']}, ValidTypes: {valid_types}",
            "num_electrodes": f"Number of Electrodes specified as active does not match Pos and Neg Electrodes Supplied, \n\t\
            Passed: {attrs['NumElectrodes']}, PosElectrodes: {attrs['Pos_Electrodes']}, Neg_Electrodes: {attrs['Neg_Electrodes']}",
            "amplitude_toohigh": f"Amplitude passed exceeds 100%\n\t, Amplitude: {attrs['Amplitude (%)']}",
        }

        if attrs["Type"] not in valid_types:
            errors.append(err_msgs["type"])

        if attrs["Amplitude"] > 100:
            errors.append(err_msgs["amplitude_toohigh"])

        # TODO: figure out the electrode hexadecimal representation
        # Pos/Neg Electrodes is a binary representation where each index is an electrode
        # if attrs["NumElectrodes"] == 2 and (
        #     attrs["Pos_Electrodes"] not in two_electrode_vals
        #     or attrs["Neg_Electrodes"] not in two_electrode_vals
        # ):
        #     errors.append(err_msgs["num_electrodes"])
        # elif attrs["NumElectrodes"] == 3 and (
        #     attrs["Pos_Electrodes"] not in three_electrode_vals
        #     or attrs["Neg_Electrodes"] not in three_electrode_vals
        # ):
        #     errors.append(err_msgs["num_electrodes"])

        if len(errors) != 0:
            raise ValueError(
                f"Timeslot_{self.id} has invalid attributes: Errors: {errors}"
            )

    @property
    def duration(self) -> int:
        return self.attribs["Duration (us)"] + self.attribs["DeadTime (us)"]


def self_test(VERBOSE=False):
    print("TIMESLOT self_test()")

    from pprint import pprint
    from Workout_sample import bionic_quads

    TS1 = TimeSlot(bionic_quads.split("\n")[4])  # First Timeslot from BionicQuads

    output_data = TS1.to_dict()

    if VERBOSE:
        print(TS1)
        pprint(output_data)


if __name__ == "__main__":
    try:
        self_test()
    except AssertionError:
        print(AssertionError)
