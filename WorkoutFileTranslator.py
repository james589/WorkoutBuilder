from Workout_Builder import Workout
from Workout import Workout
from Phase import Phase
from Pulse import Pulse
from Timeslot import TimeSlot
from misc_funcs import (
    print_header,
    remove_empty_space,
    delim_listofstr,
    clean_yaml_ordereddict_str,
)
from Workout_sample import workout_full_1, pulse_teststring_1


def analyse_workoutfile(s, line_sep: str = "\n", sep: str = ",") -> dict:
    unique_terms = dict()  # Dictionary of the unique workout
    for line in s.split(line_sep):
        # print(f"{i}:{line}")
        line_items = [item.strip() for item in line.split(sep)]
        line_type = line_items[0][:2]

        if line_type not in unique_terms:
            unique_terms[line_type] = len(line_items) - 1

    return unique_terms


def update_old_wofile(fname):
    """Take a non human readable workout file and translate to the new format"""
    pass


def main() -> None:

    # """
    # WO = Workout, 3 items
    #   PH = Phase, 11 items
    #       PL = Pulse, 4 items
    #           TS = Timeslot, 7 items
    # """

    term_elements = {"WO": Workout, "PH": Phase, "PL": Pulse, "TS": TimeSlot}

    unique_terms = analyse_workoutfile(workout_full_1)

    for line_type in unique_terms.keys():
        print(f"LineType: {line_type}, Items: {unique_terms[line_type]}")


if __name__ == "__main__":
    main()
