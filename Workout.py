from misc_funcs import (
    remove_empty_space,
    delim_listofstr,
    clean_yaml_ordereddict_str,
    read_workout_line,
)
from Phase import Phase
from collections import OrderedDict
import yaml


class Workout:
    """
    NumWorkoutPhases (integer)  [this is a division of the overall session, e.g. a ‘warm-up’, main and warm-down part]
    Duration (s), (fp)  [≤ 2 hours]
    Max current (mA), (fp)  [≤ 200mA; there is a hard-limit in the firmware that cannot be overwritten by the parameter level.  There is also a hard-limit of 140V].

    EXAMPLE:
    WO,1,600,200
    PH1,300,0,90,90,0,90,90,0,0,4.0,7
    PL1,B,100,1,2000
    TS1,C,300,5,30,4,88,22
    PL2,B,100,1,500
    TS1,C,400,5,35,2,80,08
    PL3,B,100,6,60000
    TS1,C,180,5,50,4,A0,50
    TS2,C,180,5,50,2,80,40
    TS3,C,180,5,50,2,80,10
    TS4,D,30,5,50,2,10,80
    TS5,C,180,5,50,2,40,20
    TS6,C,180,5,50,2,40,80
    """

    identifier = "WO"
    terms = ["Num_Phases", "Duration (s)", "MaxCurrent (mA)"]

    term_types = {"Num_Phases": int, "Duration (s)": float, "MaxCurrent (mA)": float}

    def __init__(self, s: str, line_sep: str = "\n", sep: str = ",") -> None:

        # file cleanup
        if type(s) == str:
            s = remove_empty_space(s)
            s_lines = s.split(line_sep)
        elif type(s) == list and type(s[0] == str):
            s_lines = s
        else:
            raise TypeError("s can be a string or a list of strings")

        # Handle "FLIPPING" line if present
        if s_lines[-1].upper() == "FLIPPING":
            self.flipping = True
            s_lines = s_lines[:-1]
        else:
            self.flipping = False

        # Parse in the workout lines
        try:
            self.id, self.attribs = read_workout_line(
                s_lines[0], self.identifier, self.terms, sep
            )
            phases_str = delim_listofstr(s_lines[1:], Phase.identifier)
            self.phases = [Phase(phase_str) for phase_str in phases_str]

        except ValueError as err:
            print(f"Error: {err}")

        # Cast the attribute strings read in, into the correct type
        temp = OrderedDict()
        for attrib_name, attrib in self.attribs.items():
            temp[attrib_name] = self.term_types[attrib_name](attrib)
        self.attribs = temp

        # Check the workout is valid
        try:
            self.valid()
        except AssertionError as err:
            print(f"Invalid Workout: {self.id} - Errors: {err}")

    def __len__(self) -> int:
        return len(self.phases)

    def __repr__(self) -> str:

        return "\n".join(
            [
                f"Workout{self.id}: ",
                ", ".join([f"{self.attribs[term]} {term}" for term in self.terms])
                + f", Flipping: {self.flipping}\n",
                *[str(phase) for phase in self.phases],
            ]
        )

    def valid(self) -> bool:
        """Check the workout is valid

        Current Checks:
        Num Phases matches Workout desc
        Phases are consectively numbered
        """

        valid = True
        errors = list()

        # Consecutive Phase numbers
        pulse_nums = [phase.id for phase in self.phases]
        if pulse_nums != [i + 1 for i in range(len(self.phases))]:
            valid = False
            errors.append("Phase ids in Workout are not consecutive")

        # Num Phases match Workout desc
        if len(self.phases) > self.attribs["Num_Phases"]:
            valid = False
            errors.append(
                "There are more Phases present in the workout than is declared on the workout desc line"
            )

        if len(self.phases) < self.attribs["Num_Phases"]:
            valid = False
            errors.append(
                "There are less Phases present in the workout than is declared on the workout desc line"
            )

        if not valid:
            raise AssertionError(errors)

    @property
    def phases_dict(self) -> dict:
        phases_dict = OrderedDict()
        for phase in self.phases:
            phases_dict.update(phase.to_dict())
        return phases_dict

    def to_dict(self) -> dict:
        output = OrderedDict(**self.attribs).copy()
        output["Phases"] = self.phases_dict
        # return dict([['Workout', output]])
        return OrderedDict([["Workout", output]])

    def to_yaml(self, simplify=True) -> None:
        """
        Convert the basic attribute of the Workout to a yaml string
        If simplify is true it will convert the ordered dict syntax into standard dict
        """
        output_data = self.to_dict()
        yaml_str = yaml.dump(
            output_data,
            stream=None,
            default_style="'",
            default_flow_style=False,
            indent=1,
            width=2,
        )

        if simplify:
            clean_yaml_str = clean_yaml_ordereddict_str(
                yaml_str=yaml_str,
                prepended_spaces_tormv=3,
                substrs=["!!python/object/apply:collections.OrderedDict"],
            )
            return clean_yaml_str

    # file IO should not be handled by the class
    # def write_to_yaml(self, fname="Workout.yaml", simplify=True):
    #     """
    #     Write basic workout attributes to a yaml file.
    #     If simplify is true it will convert the ordered dict syntax into standard dict
    #     """
    #     yaml_str = self.to_yaml(simplify)
    #     with open(fname, "w+") as f:
    #         f.write(yaml_str)
