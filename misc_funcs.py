from itertools import groupby
from collections import OrderedDict
import os


def output_workout_obj_repr_to_file(
    obj: object,
    fpath: str,
    output_subfolder: str,
    extension: str,
) -> None:

    """Takes a workout object and outputs the humanreadable version
    in a subfolder beside the original file.
    Overwrites any preexisting file with the same name"""
    ### Output human readable format to txt file

    subdir = os.sep.join([os.path.dirname(fpath), output_subfolder])
    if not os.path.exists(subdir):
        # if the demo_folder directory is not present
        # then create it.
        os.makedirs(subdir)

    fout_name = os.sep.join(
        [
            subdir,
            os.path.splitext(os.path.basename(fpath))[0]
            + extension,  # gets the filename without extension
        ]
    )

    with open(fout_name, "w") as f:
        f.write(repr(obj))
    print(f"Human Readable output written to: \n'{fout_name}'")


def read_in_workoutfile(
    workout, workoutfolder=r"Workouts", VERBOSE=True
) -> list[str, str]:

    """Reads in workout file from within the python script directory"""
    basepath = os.path.dirname(__file__)  # gets the directory of the main python file.

    if VERBOSE:
        print(f"Loaded '{os.path.basename(workout)}'")

    fpath = os.sep.join([basepath, workoutfolder, workout])
    with open(fpath, "r") as f:
        s = f.read()
    return s, fpath


def read_workout_line(s: str, identifier: str, terms: list, sep: str = ",") -> tuple:
    """Takes a string describing the command and reads it"""

    s = s.strip()

    if s == "":
        raise ValueError(f"Empty string passsed")

    if not s.startswith(identifier):
        raise ValueError(
            f"Line Identifier found does not match the identifier passed\n\
        Expected: {identifier}, Line: {s}"
        )

    s_items = s.split(sep)

    expected_num_items = len(terms)
    found_num_items = len(s_items) - 1
    if found_num_items != expected_num_items:
        # TODO: use the Correct Error Type Here
        raise ValueError(
            f"ERROR: line does not have the expected number of attributes. \n\
        Expected: {expected_num_items}, Found: {found_num_items}\n\
        Incorrect Line: {s}"
        )

    id = s_items[0][len(identifier) :].strip()  # number describing the timeslot
    try:
        id = int(id)
    except Exception:
        pass
    attribs = OrderedDict(zip(terms, s_items[1:]))

    return id, attribs


def print_header(text, width=39):
    print(" ")
    print("-" * width)
    print(text.upper().center(width - 1, " "))
    print("-" * width)


def delim_listofstr(s_list: list, delim: str) -> list:
    """
    Takes a list of strings and splits it into several lists of lists delimited by the string containing a substring
    """

    class GroupbyHelper(object):
        def __init__(self, val):
            self.val = val
            self.i = 0

        def __call__(self, val):
            self.i += self.val in val
            return self.i

    s_lines_split = [list(g) for k, g in groupby(s_list, key=GroupbyHelper(delim))]
    return s_lines_split


def split_listofstrings_bysubstring(
    s_list: list, substring: str, delim_type: str = "contains"
) -> list:
    """
    Takes a list of strings and splits it into several lists of lists delimited by the string containing a substring
    delim_type: 'contains', 'startswith', 'endswith'
    """

    if delim_type == "contains":
        return [
            list(g) for k, g in groupby(s_list, key=lambda s: substring in s) if not k
        ]

    if delim_type == "startswith":
        return [
            list(g)
            for k, g in groupby(s_list, key=lambda s: s.startswith(substring))
            if not k
        ]

    elif delim_type == "endswith":
        return [
            list(g)
            for k, g in groupby(s_list, key=lambda s: s.endswith(substring))
            if not k
        ]

    else:
        raise ValueError(
            f"Parameter passed to delim_type was not recognised. Passed: {delim_type}"
        )


def split_listofstrings_bysubstring_groupby_BASIC(s_list: list, substring: str) -> list:
    return [list(g) for k, g in groupby(s_list, key=lambda s: substring in s) if not k]


def remove_empty_space(s: str, line_sep: str = "\n") -> str:
    """
    Drops empty lines and removes whitespace
    """
    l = [line.strip() for line in s.split(line_sep) if not line.strip() == ""]
    return line_sep.join(l)


def clean_yaml_ordereddict_str(
    yaml_str: str,
    prepended_spaces_tormv: int = 0,
    substrs: list = list(),
    whitespace_divider: int = 2,
) -> str:
    """Take a NestedOrderedDict yaml str and convert to a standard dict yaml format"""

    import re

    clean_str = [  # remove lines that contain any of the substrs
        line[prepended_spaces_tormv:]
        for line in yaml_str.split("\n")
        if not any(substr in line for substr in substrs)
    ]

    # remove empty whitespace from start of each
    clean_str = [line[prepended_spaces_tormv:] for line in clean_str]
    # convert list of strs into newline seperated string
    clean_str = "\n".join(clean_str)

    # convert ordered dict header keys to standard dict format
    for keyword in ["Workout", "Phases", "Pulses", "TimeSlots"]:
        clean_str = clean_str.replace(f"'{keyword}'", f"'{keyword}':")
    for i in range(100):
        clean_str = clean_str.replace(f"_{i}'", f"_{i}':")
    # convert custom type yaml key format to standard dict key format
    clean_str = re.sub(r"\n\s*- '", ": '", clean_str)

    # drop block formatting for values
    clean_str = clean_str.replace("- ", "  ")
    # reduce whitespace by 2x
    clean_str = clean_str.replace(" " * whitespace_divider, " ")

    return clean_str


def basic_drop_fext(fname):
    return ".".join(fname.split(".")[:-1])


def test_split_listofstrings_bysubstring():
    s_lines = [
        "TS3,C,180,5,50,2,08,01",
        "PL1,B,100,1,2000",
        "TS1,C,300,5,30,4,88,22",
        "PL2,B,100,1,500",
        "TS1,C,400,5,35,2,80,08",
        "TS2,C,180,5,50,2,08,04",
        "TS3,C,180,5,50,2,08,01",
    ]

    s_expected_output = [
        ["TS3,C,180,5,50,2,08,01"],
        ["TS1,C,300,5,30,4,88,22"],
        ["TS1,C,400,5,35,2,80,08", "TS2,C,180,5,50,2,08,04", "TS3,C,180,5,50,2,08,01"],
    ]

    substring = "PL"
    s_lines_split = split_listofstrings_bysubstring(s_lines, substring)
    # print("Input: ", s_lines, sep="\n", end="\n\n")
    # print("Output: ", *s_lines_split, sep="\n")

    assert s_lines_split == s_expected_output
    # print("PASS - split_listofstrings_bysubstring(): Output is as expected")


def test_basic_drop_fext():
    tests_results = {"abc.defg.xml": "abc.defg", "test.goat": "test"}
    for test, result in tests_results.items():
        assert result == basic_drop_fext(test)


def test_delim_listofstr():
    s_lines = [
        "TS3,C,180,5,50,2,08,01",
        "PL1,B,100,1,2000",
        "TS1,C,300,5,30,4,88,22",
        "PL2,B,100,1,500",
        "TS1,C,400,5,35,2,80,08",
        "TS2,C,180,5,50,2,08,04",
        "TS3,C,180,5,50,2,08,01",
    ]

    s_expected_output = [
        ["TS3,C,180,5,50,2,08,01"],
        ["PL1,B,100,1,2000", "TS1,C,300,5,30,4,88,22"],
        [
            "PL2,B,100,1,500",
            "TS1,C,400,5,35,2,80,08",
            "TS2,C,180,5,50,2,08,04",
            "TS3,C,180,5,50,2,08,01",
        ],
    ]
    substring = "PL"
    assert delim_listofstr(s_lines, substring) == s_expected_output


def indent_paragraph(paragraph: str, indents: int = 1) -> str:
    return "\t" * indents + paragraph.replace("\n", "\n" + "\t" * indents)


def test_indent_paragraph(VERBOSE=False):
    for N in range(5):
        paragraph = "\n".join(
            ["This is a test", "This should be three lines", f"Indented {N} times"]
        )
        par_returned = indent_paragraph(paragraph, indents=N)

        par_returned_split = par_returned.split("\n")
        for i in range(3):
            assert par_returned_split[i].startswith("\t" * N)

        if VERBOSE:
            print(par_returned)


def run_tests():
    test_split_listofstrings_bysubstring()
    test_basic_drop_fext()
    test_delim_listofstr()
    test_indent_paragraph()

    print("misc_funcs.py: PASSED ALL TESTS")


def main():
    run_tests()


if __name__ == "__main__":
    main()
